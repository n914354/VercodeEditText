# VercodeEditText

本项目是基于开源项目 VercodeEditText 进行移植和开发的，可以通过项目标签以及github地址（ https://github.com/JingYeoh/VercodeEditText ）追踪到原安卓项目版本


### 项目介绍
- 项目名称：VercodeEditText
- 所属系列：OpenHarmony的第三方组件适配移植
- 功能：一个验证码输入框控件
- 项目移植状态：100%
- 调用差异：无
- 项目作者和维护人：杜波
- 联系方式：bo.du@archermind.com
- 开发版本：sdk5，DevEco Studio2.1 beta1
- 原项目Doc地址：https://github.com/JingYeoh/VercodeEditText

### 项目介绍

- 编程语言：Java
- 一个验证码输入框控件

### 安装教程

**在 Project 的 build.gradle 下添加 jcenter 仓库**

```
repositories {
    mavenCentral()
}
```

**在 Module 的 build.gradle 下添加 VercodeEditText 依赖**

```
implementation 'com.gitee.archermind-ti:vcedittext_lib:1.0.0'
```



### 使用说明
#### 在布局中使用
```html
    <com.jkb.vcedittext.VerificationCodeEditText
        ohos:id="$+id:am_et"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:min_height="50vp"
        ohos:margin="20vp"
        ohos:text_input_type="pattern_number"
        ohos:text="123"
        ohos:text_color="$color:colorPrimary"
        ohos:text_size="40fp"
        app:bottomLineHeight="2vp"
        app:bottomLineNormalColor="$color:gravy_light"
        app:bottomLineSelectedColor="$color:colorAccent"
        app:cursorColor="$color:holo_purple"
        app:figures="4"
        app:selectedBackgroundColor="$color:colorPrimary_alpha33"
        app:verCodeMargin="10vp"
        app:backgroundColor = "#ffffff"/>
```



#### 属性说明

属性|介绍|取值
---|---|---
figures|验证码位数|integer
verCodeMargin|每个验证码的间隔|dimension
bottomLineSelectedColor|底线选择状态下的颜色|color
bottomLineNormalColor|底线未选中状态下的颜色|color
bottomLineHeight|底线高度|dimension
selectedBackgroundColor|选中的背景颜色|color
cursorDuration|光标闪烁间隔时间|integer
cursorColor|光标颜色|color
cursorWidth|光标宽度|dimension
backgroundColor|背景颜色|color


#### 方法说明
返回|方法
---|---
void|setFigures(int figures)
void|setVerCodeMargin(int margin)
void|setBottomSelectedColor(int bottomSelectedColor)
void|setBottomNormalColor(int bottomNormalColor)
void|setSelectedBackgroundColor(int selectedBackground)
void|setBottomLineHeight(int bottomLineHeight)
void|setBackgroundColor(int backgroundColor)
void|setOnVerificationCodeChangedListener(OnVerificationCodeChangedListener listener)


### 局限性
- backgroundColor不能取透明


###  版本迭代
- v1.0.0

###  版权和许可信息
  - Apache License Version 2.0

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.