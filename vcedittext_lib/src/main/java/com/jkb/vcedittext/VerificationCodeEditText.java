package com.jkb.vcedittext;


import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Timer;
import java.util.TimerTask;


/**
 * 验证码的EditText
 * Created by yj on 2017/6/12.
 */

public class VerificationCodeEditText extends TextField implements
        VerificationAction, Text.TextObserver, Component.EstimateSizeListener, Component.BindStateChangedListener,
        Component.TouchEventListener, Component.DrawTask {
    private static final int DEFAULT_CURSOR_DURATION = 400;

    private int mFigures=4;//需要输入的位数
    private int mVerCodeMargin=0;//验证码之间的间距
    private int mBottomSelectedColor=2730;//底部选中的颜色
    private int mBottomNormalColor=2730;//未选中的颜色
    private float mBottomLineHeight=5;//底线的高度
    private int mSelectedBackgroundColor=2730;//选中的背景颜色
    private int mCursorWidth=1;//光标宽度
    private int mCursorColor=2730;//光标颜色
    private int mCursorDuration =DEFAULT_CURSOR_DURATION;//光标闪烁间隔
    private int mBackgroundColor = Color.WHITE.getValue();//背景颜色不能透明

    private OnVerificationCodeChangedListener onCodeChangedListener;
    private int mCurrentPosition = 0;
    private int mEachRectLength = 0;//每个矩形的边长
    private Paint mSelectedBackgroundPaint;
    private Paint mNormalBackgroundPaint;
    private Paint mBottomSelectedPaint;
    private Paint mBottomNormalPaint;
    private Paint mCursorPaint;
    private Paint mTextPaint;
    private Paint mBgPaint;

    // 控制光标闪烁
    private boolean isCursorShowing =false;
    private TimerTask mCursorTimerTask;
    private Timer mCursorTimer;
    //


    public VerificationCodeEditText(Context context) {
        this(context, null);
    }

    public VerificationCodeEditText(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public VerificationCodeEditText(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        initAttrs(attrs);

        initPaint();
        initCursorTimer();
        setTouchFocusable(true);
        addTextObserver(this);
        setEstimateSizeListener(this);
        setBindStateChangedListener(this);
        setTouchEventListener(this);
        addDrawTask(this,DrawTask.BETWEEN_CONTENT_AND_FOREGROUND);
        //屏蔽原生显示
        setTextCursorVisible(false);

    }

    /**
     * 初始化paint
     */
    private void initPaint() {
        mBgPaint = new Paint();
        mBgPaint.setColor(new Color(mBackgroundColor));

        mSelectedBackgroundPaint = new Paint();
        mSelectedBackgroundPaint.setColor(new Color(mSelectedBackgroundColor));
        mNormalBackgroundPaint = new Paint();
        mNormalBackgroundPaint.setColor(Color.TRANSPARENT);

        mBottomSelectedPaint = new Paint();
        mBottomNormalPaint = new Paint();
        mBottomSelectedPaint.setColor(new Color(mBottomSelectedColor));
        mBottomNormalPaint.setColor(new Color(mBottomNormalColor));
        mBottomSelectedPaint.setStrokeWidth(mBottomLineHeight);
        mBottomNormalPaint.setStrokeWidth(mBottomLineHeight);

        mCursorPaint = new Paint();
        mCursorPaint.setAntiAlias(true);
        mCursorPaint.setColor(new Color(mCursorColor));
        mCursorPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        mCursorPaint.setStrokeWidth(mCursorWidth);

        mTextPaint = new Paint();
        mTextPaint.setTextAlign(TextAlignment.CENTER);
        mTextPaint.setTextSize(getTextSize());
        mTextPaint.setColor(getTextColor());

    }

    /**
     * 初始化Attrs
     */
    private void initAttrs(AttrSet attrs) {
        if(attrs.getAttr("figures").isPresent()){
            mFigures = attrs.getAttr("figures").get().getIntegerValue();
        }
        if(attrs.getAttr("verCodeMargin").isPresent()){
            mVerCodeMargin = attrs.getAttr("verCodeMargin").get().getDimensionValue();
        }
        if(attrs.getAttr("bottomLineSelectedColor").isPresent()){
            mBottomSelectedColor = attrs.getAttr("bottomLineSelectedColor").get().getColorValue().getValue();
        }
        if(attrs.getAttr("bottomLineNormalColor").isPresent()){
            mBottomNormalColor = attrs.getAttr("bottomLineNormalColor").get().getColorValue().getValue();
        }
        if(attrs.getAttr("bottomLineHeight").isPresent()){
            mBottomLineHeight = attrs.getAttr("bottomLineHeight").get().getDimensionValue();
        }
        if(attrs.getAttr("selectedBackgroundColor").isPresent()){
            mSelectedBackgroundColor = attrs.getAttr("selectedBackgroundColor").get().getColorValue().getValue();
        }
        if(attrs.getAttr("cursorWidth").isPresent()){
            mCursorWidth = attrs.getAttr("cursorWidth").get().getDimensionValue();
        }

        if(attrs.getAttr("cursorColor").isPresent()){
            mCursorColor = attrs.getAttr("cursorColor").get().getColorValue().getValue();
        }
        if(attrs.getAttr("cursorDuration").isPresent()){
            mCursorDuration = attrs.getAttr("cursorDuration").get().getIntegerValue();
        }
        if(attrs.getAttr("backgroundColor").isPresent()){
            mBackgroundColor = attrs.getAttr("backgroundColor").get().getColorValue().getValue();
        }

    }

    private void initCursorTimer() {
        mCursorTimerTask = new TimerTask() {
            @Override
            public void run() {
                // 通过光标间歇性显示实现闪烁效果
                isCursorShowing = !isCursorShowing;
                //invalidate();
                getContext().getUITaskDispatcher().syncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        invalidate();
                    }
                });

            }
        };
        mCursorTimer = new Timer();

    }




    @Override
    public void onTextUpdated(String s, int start, int before, int count) {

        if (onCodeChangedListener != null) {
            onCodeChangedListener.onVerCodeChanged(s, start, before, count);
        }
        if (getText().length() == mFigures) {
            if (onCodeChangedListener != null) {
                onCodeChangedListener.onInputCompleted(getText());
            }
        }if (getText().length() > mFigures) {
            delete(getText().length()-mFigures);
            return;
        }
        mCurrentPosition = getText().length();
    }




    @Override
    public void setFigures(int figures) {
        mFigures = figures;
        invalidate();
    }

    @Override
    public void setVerCodeMargin(int margin) {
        mVerCodeMargin = margin;
        invalidate();
    }

    @Override
    public void setBottomSelectedColor( int bottomSelectedColor) {
        mBottomSelectedColor = getColor(bottomSelectedColor);
        initPaint();
        invalidate();
    }

    @Override
    public void setBottomNormalColor( int bottomNormalColor) {
        mBottomNormalColor = getColor(bottomNormalColor);
        initPaint();
        invalidate();
    }

    @Override
    public void setSelectedBackgroundColor( int selectedBackground) {
        mSelectedBackgroundColor = getColor(selectedBackground);
        initPaint();
        invalidate();
    }

    @Override
    public void setBottomLineHeight(int bottomLineHeight) {
        this.mBottomLineHeight = bottomLineHeight;
        initPaint();
        invalidate();
    }

    public void setBackgroundColor(int backgroundColor){
        this.mBackgroundColor = getColor(backgroundColor);
        initPaint();
        invalidate();
    }

    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        initPaint();
        invalidate();
    }

    @Override
    public void setOnVerificationCodeChangedListener(OnVerificationCodeChangedListener listener) {
        this.onCodeChangedListener = listener;
    }

    /**
     * 返回颜色
     */
    private int getColor( int color) {
        try {
            return getResourceManager().getElement(color).getColor();
        }catch (Exception e){

        }
        return 0;
    }


    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int widthResult = 0, heightResult = 0;
        //最终的宽度
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        if (widthMode == MeasureSpec.PRECISE) {
            widthResult = widthSize;
        } else {
            widthResult = getScreenWidth();
        }
        //每个矩形形的宽度
        mEachRectLength = (widthResult - (mVerCodeMargin * (mFigures - 1))) / mFigures;
        //最终的高度
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if (heightMode == MeasureSpec.PRECISE) {
            heightResult = heightSize;
        } else {
            heightResult = mEachRectLength;
        }

        setEstimatedSize(widthResult, heightResult);
        return false;
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        // 启动定时任务，定时刷新实现光标闪烁
        mCursorTimer.scheduleAtFixedRate(mCursorTimerTask, 0, mCursorDuration);
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        mCursorTimer.cancel();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            requestFocus();
            //setSelection(getText().length());
            //showKeyBoard(getContext());
            return false;
        }
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mCurrentPosition = getText().length();
        int width = mEachRectLength - getPaddingLeft() - getPaddingRight();
        int height = getEstimatedHeight() - getPaddingTop() - getPaddingBottom();
        //绘制背景
        canvas.save();
        canvas.drawRect(0,0,getEstimatedWidth(),getEstimatedWidth(),mBgPaint);
        canvas.restore();
        //绘制矩形
        for (int i = 0; i < mFigures; i++) {
            canvas.save();
            int start = width * i + i * mVerCodeMargin;
            int end = width + start;
            //画一个矩形
            if (i == mCurrentPosition) {//选中的下一个状态
                canvas.drawRect(start, 0, end, height, mSelectedBackgroundPaint);
            } else {
                canvas.drawRect(start, 0, end, height, mNormalBackgroundPaint);
            }

            canvas.restore();
        }
        //绘制文字
        String value = getText();
        for (int i = 0; i < value.length(); i++) {
            canvas.save();
            int start = width * i + i * mVerCodeMargin;
            float x = start + width / 2;

            Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
            float baseline = (height - fontMetrics.bottom + fontMetrics.top) / 2
                    - fontMetrics.top;
            canvas.drawText(mTextPaint,String.valueOf(value.charAt(i)), x, baseline);
            canvas.restore();
        }
        //绘制底线
        for (int i = 0; i < mFigures; i++) {
            canvas.save();
            float lineY = height - mBottomLineHeight / 2;
            int start = width * i + i * mVerCodeMargin;
            int end = width + start;
            if (i < mCurrentPosition) {
                canvas.drawLine(start, lineY, end, lineY, mBottomSelectedPaint);
            } else {
                canvas.drawLine(start, lineY, end, lineY, mBottomNormalPaint);
            }
            canvas.restore();
        }
        //绘制光标
        if (!isCursorShowing && true && mCurrentPosition < mFigures && hasFocus()) {
            canvas.save();
            int startX = mCurrentPosition * (width + mVerCodeMargin) + width / 2;
            int startY = height / 4;
            int endX = startX;
            int endY = height - height / 4;
            canvas.drawLine(startX, startY, endX, endY, mCursorPaint);
            canvas.restore();
        }

    }





    /**
     * 获取手机屏幕的宽度
     */
    private int getScreenWidth() {

        return getResourceManager().getDeviceCapability().width;
    }


}
